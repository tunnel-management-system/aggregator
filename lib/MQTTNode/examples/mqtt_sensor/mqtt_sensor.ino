/*
  MQTT sensor operation example.
  
  Uses MQTT client to read sensor value and publish to OUT TOPIC.
  Subscribes to IN TOPIC for timer configuration. Command:
  * SET TIMER <TIMER_MS>
*/

#include <WiFi.h>
#include <MQTT.h>
#include <MQTTNode.h>

/* Network connection settings */
const char* ssid              = "GL-MT300N-V2-5fa";
const char* password          = "goodlife";

/* MQTT settings */
const char* mqtt_server       = "192.168.8.1";
const int mqtt_port           = 1883;

// WiFi client
WiFiClient espWiFiClient;

// MQTT client
MQTTClient client;

// MQTT sensor
MQTTSensor<int> Sensor("id0001", 1000, "topic/in", "topic/out");

// Define global variable to save values
int rndValue;


void setup()
{
  // Initialize serial communication
  Serial.begin(115200);

  // Connecting to the WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print('.');
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // MQTT client connect
  client.begin(mqtt_server, mqtt_port, espWiFiClient);

  // MQTT sensor setup
  Sensor.begin(client);
  Sensor.setVariable(&rndValue);
  
}

void loop()
{
  // Check connection to MQTT server
  if (!Sensor.connected()) {
    // Reconnect and subscribe in topic
    Sensor.reconnect();
  }

  rndValue = (int)random(10);

  Sensor.loop();  // Process in calls
}