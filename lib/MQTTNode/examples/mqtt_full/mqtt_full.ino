/*
  MQTT full example.

  Instantiates multiple actuator/sensor MQTT nodes and stores in a node container.
  Actuators subscribe to IN topic for operation commands.
  Sensors publish data to OUT topic. Additionally, they subscribe to IN topic for TIMER parameter setup.
  
*/

#include <WiFi.h>
#include <MQTT.h>
#include <MQTTNode.h>

/* Network connection settings */
const char* ssid              = "GL-MT300N-V2-5fa";
const char* password          = "goodlife";

/* MQTT settings */
const char* mqtt_server       = "192.168.8.1";
const int mqtt_port           = 1883;

// WiFi clients
WiFiClient espWiFiClient_1, espWiFiClient_2, espWiFiClient_3, 
           espWiFiClient_4, espWiFiClient_5, espWiFiClient_6, 
           espWiFiClient_7, espWiFiClient_8, espWiFiClient_9, 
           espWiFiClient_10;

// MQTT clients
MQTTClient MQTTclient_1, MQTTclient_2, MQTTclient_3, MQTTclient_4, 
           MQTTclient_5, MQTTclient_6, MQTTclient_7, MQTTclient_8, 
           MQTTclient_9, MQTTclient_10;

// MQTT nodes
// Node#1 ---> Built-in LED
LEDActuator LED1("L0001", 2, "CENTER/1/SECTION/1/L0001/SET", "CENTER/1/SECTION/1/L0001");
// Node#2 ---> LED
LEDActuator LED2("L0002", 17, "CENTER/1/SECTION/1/L0002/SET", "CENTER/1/SECTION/1/L0002");
// Node#3 ---> LED
LEDActuator LED3("L0003", 25, "CENTER/1/SECTION/1/L0003/SET", "CENTER/1/SECTION/1/L0003");
// Node#4 ---> Potentiometer
MQTTSensor<float> Sensor1("S0001", 1000, "CENTER/1/SECTION/1/S0001/SET", "CENTER/1/SECTION/1/S0001");
// Node#5 ---> Temperature Sensor (DHT22)
MQTTSensor<float> Sensor2("S0002", 1000, "CENTER/1/SECTION/1/S0002/SET", "CENTER/1/SECTION/1/S0002");
// Node#6 ---> Humidity Sensor (DHT22)
MQTTSensor<float> Sensor3("S0003", 1000, "CENTER/1/SECTION/1/S0003/SET", "CENTER/1/SECTION/1/S0003");
// Node#7 ---> Analog Gas Sensor (MQ2)
MQTTSensor<float> Sensor4("S0004", 1000, "CENTER/1/SECTION/1/S0004/SET", "CENTER/1/SECTION/1/S0004");
// Node#8 ---> Analog CO Sensor (MQ7)
MQTTSensor<float> Sensor5("S0005", 1000, "CENTER/1/SECTION/1/S0005/SET", "CENTER/1/SECTION/1/S0005");
// Node#9 ---> Analog High Temperature Sensor
MQTTSensor<float> Sensor6("S0006", 1000, "CENTER/1/SECTION/1/S0006/SET", "CENTER/1/SECTION/1/S0006");
// Node#10 ---> Digital PIR (Motion) Sensor
MQTTSensor<float> Sensor7("S0007", 1000, "CENTER/1/SECTION/1/S0007/SET", "CENTER/1/SECTION/1/S0007");

// MQTT node container
MQTTNodeContainer Container(10, &LED1, &LED2, &LED3, &Sensor1, &Sensor2, &Sensor3, &Sensor4, &Sensor5, &Sensor6, &Sensor7);

// Global variables for sensor data
float potentiometer, temperature, humidity, analogGas, analogCO, analogHighTemperature, motion;


void setup()
{
  // Initialize serial communication
  Serial.begin(115200);

  // Connecting to the WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print('.');
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // MQTT client connect
  MQTTclient_1.begin(mqtt_server, mqtt_port, espWiFiClient_1);
  MQTTclient_2.begin(mqtt_server, mqtt_port, espWiFiClient_2);
  MQTTclient_3.begin(mqtt_server, mqtt_port, espWiFiClient_3);
  MQTTclient_4.begin(mqtt_server, mqtt_port, espWiFiClient_4);
  MQTTclient_5.begin(mqtt_server, mqtt_port, espWiFiClient_5);
  MQTTclient_6.begin(mqtt_server, mqtt_port, espWiFiClient_6);
  MQTTclient_7.begin(mqtt_server, mqtt_port, espWiFiClient_7);
  MQTTclient_8.begin(mqtt_server, mqtt_port, espWiFiClient_8);
  MQTTclient_9.begin(mqtt_server, mqtt_port, espWiFiClient_9);
  MQTTclient_10.begin(mqtt_server, mqtt_port, espWiFiClient_10);

  // MQTT node setup
  LED1.begin(MQTTclient_1);
  LED2.begin(MQTTclient_2);
  LED3.begin(MQTTclient_3);
  Sensor1.begin(MQTTclient_4);
  Sensor2.begin(MQTTclient_5);
  Sensor3.begin(MQTTclient_6);
  Sensor4.begin(MQTTclient_7);
  Sensor5.begin(MQTTclient_8);
  Sensor6.begin(MQTTclient_9);
  Sensor7.begin(MQTTclient_10);

  // Set sensor variables
  Sensor1.setVariable(&potentiometer);
  Sensor2.setVariable(&temperature);
  Sensor3.setVariable(&humidity);
  Sensor4.setVariable(&analogGas);
  Sensor5.setVariable(&analogCO);
  Sensor6.setVariable(&analogHighTemperature);
  Sensor7.setVariable(&motion);
}


void loop()
{
  // Check connection to MQTT server
  if (!Container.connected()) {
    // Reconnect and subscribe in topic
    Container.reconnect();
  }

  potentiometer         = (float)random(10);
  temperature           = (float)random(10);
  humidity              = (float)random(10);
  analogGas             = (float)random(10);
  analogCO              = (float)random(10);
  analogHighTemperature = (float)random(10);
  motion                = (float)random(10);

  Container.loop();  // Process in calls
}
