/*
    MQTTSensor.cpp - MQTT sensor definition based on MQTT node object.
*/

#include "MQTTSensor.h"


template <class T>
MQTTSensor<T>::MQTTSensor(const char* id, unsigned int timer_ms)
: MQTTNode(id), _timer_ms(timer_ms)
{
    this->_last_timestamp = millis();
}

template <class T>
MQTTSensor<T>::MQTTSensor(const char* id, unsigned int timer_ms, const char* mqtt_in_topic, const char* mqtt_out_topic)
: MQTTNode(id, mqtt_in_topic, mqtt_out_topic), _timer_ms(timer_ms)
{
    this->_last_timestamp = millis();
}

template <class T>
void MQTTSensor<T>::setVariable(T* var)
{
    this->_value = var;
}

template <class T>
void MQTTSensor<T>::process(MQTTClient* client, char* topic, char* bytes, int length)
{
    MQTTNode::process(client, topic, bytes, length);

    unsigned int new_timer;
    if (sscanf(bytes, "SET TIMER %u", &new_timer)) {
        this->_timer_ms = new_timer;
    }
}

template <class T>
void MQTTSensor<T>::loop()
{
    unsigned int current_timestamp = millis();
    if (current_timestamp - this->_last_timestamp > this->_timer_ms) {
        String strVal(*(this->_value));
        this->_mqtt_client->publish(this->_mqtt_out_topic, strVal.c_str());
        this->_last_timestamp = current_timestamp;
    }

    MQTTNode::loop();
}

template class MQTTSensor<int>;
template class MQTTSensor<float>;
template class MQTTSensor<long>;
template class MQTTSensor<bool>;