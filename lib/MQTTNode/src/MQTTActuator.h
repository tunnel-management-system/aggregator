/*
    MQTTActuator.h - MQTT actuator definition based on MQTT node object.
*/

#ifndef MQTT_ACTUATOR_H
#define MQTT_ACTUATOR_H

#include "MQTTBaseNode.h"


class LEDActuator : public MQTTNode {

    const int _pin;

    public:
        LEDActuator(const char* id, int pin);
        LEDActuator(const char* id, const int pin, const char* mqtt_in_topic, const char* mqtt_out_topic);
        ~LEDActuator();

        void process(MQTTClient* client, char* topic, char* bytes, int length);

    private:
        void _on();
        void _off();
        void _blink(int n_cycles, int period_ms = 1000, float duty_cycle = 0.5);
    
};

#endif