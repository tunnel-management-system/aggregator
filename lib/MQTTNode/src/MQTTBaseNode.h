/*
    MQTTBaseNode.h - Base MQTT node definition.
*/

#ifndef MQTT_BASE_NODE_H
#define MQTT_BASE_NODE_H

#include <Arduino.h>
#include <MQTT.h>
#include <vector>


class MQTTNode {

    protected:
        const char* _id;
        const char* _mqtt_in_topic;
        const char* _mqtt_out_topic;
        MQTTClient* _mqtt_client;
        HardwareSerial* _serial;

    public:
        MQTTNode(const char* id);
        MQTTNode(const char* id, const char* mqtt_in_topic, const char* mqtt_out_topic);
        ~MQTTNode();

        void begin(MQTTClient& c, HardwareSerial& serial = Serial);

        bool connected();
        void reconnect(int attempts = 5, const char* username = nullptr, const char* password = nullptr);

        virtual void process(MQTTClient* client, char* topic, char* bytes, int length);
        virtual void loop();

    protected:
        void _log_serial(char* topic, char* payload, int length, const char* prefix = "topic");
    
};

class MQTTNodeContainer {

    std::vector<MQTTNode*> _nodes;

    public:
        MQTTNodeContainer(int count, ...);
        void add(MQTTNode& node);

        bool connected();
        void reconnect(int attempts = 5, const char* username = nullptr, const char* password = nullptr);

        void loop();

};

#endif