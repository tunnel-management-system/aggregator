/*
    MQTTSensor.h - MQTT sensor definition based on MQTT node object.
*/

#ifndef MQTT_SENSOR_H
#define MQTT_SENSOR_H

#include "MQTTBaseNode.h"


template <class T>
class MQTTSensor : public MQTTNode {

    T* _value;
    unsigned int _timer_ms;
    unsigned int _last_timestamp;

    public:
        MQTTSensor(const char* id, unsigned int timer_ms);
        MQTTSensor(const char* id, unsigned int timer_ms, const char* mqtt_in_topic, const char* mqtt_out_topic);

        void setVariable(T* var);

        void process(MQTTClient* client, char* topic, char* bytes, int length);
        void loop();
    
};

#endif