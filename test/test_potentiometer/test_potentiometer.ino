/*
  Potentiometer test. Pinout:
  * P1 ---> 5V
  * P2 ---> IO4
  * P3 ---> GND
*/

void setup()
{
  Serial.begin(115200);
}

void loop()
{
  Serial.print("Potentiometer: ");
  Serial.println(analogRead(4));
  delay(200);
}
