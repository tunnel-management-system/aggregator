/*
  DHT22 Temperature & Humidity sensor test
*/

#include <DHTesp.h>

DHTesp dht;

uint8_t pin = 26;

void setup()
{
  Serial.begin(115200);
  dht.setup(pin, DHTesp::DHT22);
}

void loop()
{
  float temperature = dht.getTemperature();
  float humidity = dht.getHumidity();

  Serial.print("Temperature: ");
  Serial.println(temperature);
  Serial.print("Humidity: ");
  Serial.println(humidity);
  Serial.println();

  delay(1000);
}
