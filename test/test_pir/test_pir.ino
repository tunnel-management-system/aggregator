/*
    PIR (Pyroelectric Infrared) Motion Sensor test.
*/

const int sensorPin = 26;


void setup()
{
    Serial.begin(115200);

    pinMode(sensorPin, INPUT);
}

void loop()
{
    int state = digitalRead(sensorPin);

    Serial.print("Detection: ");
    Serial.println(state == 1);

    delay(200);
}
