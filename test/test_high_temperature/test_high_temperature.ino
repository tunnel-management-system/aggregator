/*
    High Temperature Sensor test.
*/

#include <DFRobotHighTemperatureSensor.h>

const float voltageRef = 5.00;  // Set reference voltage

int highTemperaturePin = 4;

DFRobotHighTemperature PT100 = DFRobotHighTemperature(voltageRef);


void setup()
{
    Serial.begin(115200);
}

void loop()
{
    int temperature = PT100.readTemperature(highTemperaturePin);

    Serial.print("Temperature: ");
    Serial.println(temperature);

    delay(200);
}
