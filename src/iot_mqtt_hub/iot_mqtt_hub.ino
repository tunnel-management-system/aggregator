/*
  IoT MQTT hub based on ESP32 dev board.
  Uses MQTT client to perform actions on actuators and send sensor data.
  
*/

#include <WiFi.h>
#include <MQTT.h>
#include <MQTTNode.h>

#include <DHTesp.h>
#include <DFRobotHighTemperatureSensor.h>

#define PIN_LED1          2
#define PIN_LED2          17
#define PIN_LED3          25

#define PIN_POTENTIOMETER 4
#define PIN_DHT22         26
#define PIN_MQ2           36
#define PIN_MQ7           34
#define PIN_HIGH_TEMP     39
#define PIN_PIR           16

#define V_REF             3.3
#define TIMER_MS          1000

/* Network connection settings */
const char* ssid              = "GL-MT300N-V2-5fa";
const char* password          = "goodlife";

/* MQTT settings */
const char* mqtt_server       = "192.168.8.1";
const int mqtt_port           = 1883;

// WiFi clients
WiFiClient espWiFiClient_1, espWiFiClient_2, espWiFiClient_3, 
           espWiFiClient_4, espWiFiClient_5, espWiFiClient_6, 
           espWiFiClient_7, espWiFiClient_8, espWiFiClient_9, 
           espWiFiClient_10;

// MQTT clients
MQTTClient MQTTclient_1, MQTTclient_2, MQTTclient_3, MQTTclient_4, 
           MQTTclient_5, MQTTclient_6, MQTTclient_7, MQTTclient_8, 
           MQTTclient_9, MQTTclient_10;

// MQTT nodes
// Node#1 ---> Built-in LED
LEDActuator LED1("L0001", PIN_LED1, "CENTER/1/SECTION/1/L0001/SET", "CENTER/1/SECTION/1/L0001");
// Node#2 ---> LED
LEDActuator LED2("L0002", PIN_LED2, "CENTER/1/SECTION/1/L0002/SET", "CENTER/1/SECTION/1/L0002");
// Node#3 ---> LED
LEDActuator LED3("L0003", PIN_LED3, "CENTER/1/SECTION/1/L0003/SET", "CENTER/1/SECTION/1/L0003");
// Node#4 ---> Potentiometer
MQTTSensor<int> Sensor1("S0001", TIMER_MS, "CENTER/1/SECTION/1/S0001/SET", "CENTER/1/SECTION/1/S0001");
// Node#5 ---> Temperature Sensor (DHT22)
MQTTSensor<float> Sensor2("S0002", TIMER_MS, "CENTER/1/SECTION/1/S0002/SET", "CENTER/1/SECTION/1/S0002");
// Node#6 ---> Humidity Sensor (DHT22)
MQTTSensor<float> Sensor3("S0003", TIMER_MS, "CENTER/1/SECTION/1/S0003/SET", "CENTER/1/SECTION/1/S0003");
// Node#7 ---> Analog Gas Sensor (MQ2)
MQTTSensor<int> Sensor4("S0004", TIMER_MS, "CENTER/1/SECTION/1/S0004/SET", "CENTER/1/SECTION/1/S0004");
// Node#8 ---> Analog CO Sensor (MQ7)
MQTTSensor<int> Sensor5("S0005", TIMER_MS, "CENTER/1/SECTION/1/S0005/SET", "CENTER/1/SECTION/1/S0005");
// Node#9 ---> Analog High Temperature Sensor
MQTTSensor<float> Sensor6("S0006", TIMER_MS, "CENTER/1/SECTION/1/S0006/SET", "CENTER/1/SECTION/1/S0006");
// Node#10 ---> Digital PIR (Motion) Sensor
MQTTSensor<bool> Sensor7("S0007", TIMER_MS, "CENTER/1/SECTION/1/S0007/SET", "CENTER/1/SECTION/1/S0007");

// MQTT node container
MQTTNodeContainer Container(10, &LED1, &LED2, &LED3, &Sensor1, &Sensor2, &Sensor3, &Sensor4, &Sensor5, &Sensor6, &Sensor7);

// Sensors
DHTesp dht;
DFRobotHighTemperature PT100(V_REF);

// Global variables for sensor data
int potentiometer, analogGas, analogCO;
float temperature, humidity, analogHighTemperature;
bool motion;


void setup()
{
  // Initialize serial communication
  Serial.begin(115200);

  // Sensor initialization
  dht.setup(PIN_DHT22, DHTesp::DHT22);

  // Connecting to the WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print('.');
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // MQTT client connect
  MQTTclient_1.begin(mqtt_server, mqtt_port, espWiFiClient_1);
  MQTTclient_2.begin(mqtt_server, mqtt_port, espWiFiClient_2);
  MQTTclient_3.begin(mqtt_server, mqtt_port, espWiFiClient_3);
  MQTTclient_4.begin(mqtt_server, mqtt_port, espWiFiClient_4);
  MQTTclient_5.begin(mqtt_server, mqtt_port, espWiFiClient_5);
  MQTTclient_6.begin(mqtt_server, mqtt_port, espWiFiClient_6);
  MQTTclient_7.begin(mqtt_server, mqtt_port, espWiFiClient_7);
  MQTTclient_8.begin(mqtt_server, mqtt_port, espWiFiClient_8);
  MQTTclient_9.begin(mqtt_server, mqtt_port, espWiFiClient_9);
  MQTTclient_10.begin(mqtt_server, mqtt_port, espWiFiClient_10);

  // MQTT node setup
  LED1.begin(MQTTclient_1);
  LED2.begin(MQTTclient_2);
  LED3.begin(MQTTclient_3);
  Sensor1.begin(MQTTclient_4);
  Sensor2.begin(MQTTclient_5);
  Sensor3.begin(MQTTclient_6);
  Sensor4.begin(MQTTclient_7);
  Sensor5.begin(MQTTclient_8);
  Sensor6.begin(MQTTclient_9);
  Sensor7.begin(MQTTclient_10);

  // Set sensor variables
  Sensor1.setVariable(&potentiometer);
  Sensor2.setVariable(&temperature);
  Sensor3.setVariable(&humidity);
  Sensor4.setVariable(&analogGas);
  Sensor5.setVariable(&analogCO);
  Sensor6.setVariable(&analogHighTemperature);
  Sensor7.setVariable(&motion);
}


void loop()
{
  // Check connection to MQTT server
  if (!Container.connected()) {
    // Reconnect and subscribe in topic
    Container.reconnect();
  }

  potentiometer         = analogRead(PIN_POTENTIOMETER);
  temperature           = dht.getTemperature();
  humidity              = dht.getHumidity();
  analogGas             = analogRead(PIN_MQ2);
  analogCO              = analogRead(PIN_MQ7);
  analogHighTemperature = PT100.readTemperature(PIN_HIGH_TEMP);
  motion                = digitalRead(PIN_PIR);

  Container.loop();  // Process in calls
}
